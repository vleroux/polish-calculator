//
//  PolishOperation.m
//  Polish Calculator
//
//  Created by v.leroux on 12/01/15.
//  Copyright (c) 2015 vleroux. All rights reserved.
//

#import "PolishOperation.h"




@interface PolishOperation (){
    
}

@property (nonatomic,strong) NSString* query;

@end

@implementation PolishOperation

- (id)initWithQuery:(NSString *)query {
    
    self = [super init];
    
    if (self) {
        self.query = query;
    }
    return self;
    
}

/**
 * @return NSNumber returns the calculation of the opperations or nothing nil if unable to calculate
 */
-(NSNumber*) getResult:(NSError**)error{
    
    if (!self.query && self.query.length) {
        return nil;
    }
    
    //Validate the operation
    if (![MathOperation isStringValid:self.query handleError:error]) {
        return nil;
    }
    
    //If we have an operation
    if (self.query.length> 2 && [self startsAndEndsWithBrackets]) {
        
        unichar operation = [self.query characterAtIndex:1];
        
        //Parse the end of the operation
        
        //alloc array for all next operand;
        NSMutableArray* operands = [NSMutableArray new];
        
        
        if (self.query.length < 4) {
            *error =[PolishOperation cannotParse:self.query];
            return  nil;
        }
        NSString* tempQuery = [self.query substringFromIndex:3];
        
        BOOL endFound= NO;
        //Iterate over the query to find all the operands
        while (!endFound) {
            
            NSString* strOperand = [self findOperandInString:tempQuery handleError:error];
            
            if (strOperand==nil){ // unable to parse // we stop
                return nil;
            }
            if (!strOperand.length) {
                endFound = YES;
                
            } else {  // we continue
                NSNumber* result = [[[PolishOperation alloc]initWithQuery:strOperand] getResult:error];
                
                if (!result) {
                    return nil;
                }
                [operands addObject:result];
                tempQuery = [tempQuery substringFromIndex:1 + strOperand.length];
            }
            
        }
        
        //Return the result
        return [self calculateIntOperation:operation arrayOfNumericalOperand:operands handleError:error];
        
    } else {
        
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        NSNumber *tempResult = [numberFormatter numberFromString:self.query];
        
        if (!tempResult) {
            
            *error = [PolishOperation notANumber:self.query];
        }
        
        return tempResult;
    }
    
}

#pragma mark - Parsing

/**
 * @return BOOL is the query starts and ends with bracket ?
 */
-(BOOL)startsAndEndsWithBrackets{
    
    return [[self.query substringToIndex:1] isEqualToString:@"("] && [[self.query substringFromIndex:self.query.length-1] isEqualToString:@")"];
    
}

/**
 * @param string The string to search the operand in
 * @return The string of the next operand found or nil on error or empty if no operand
 */
-(NSString*)findOperandInString:(NSString*)string handleError:(NSError**)error{
    
    
    if(![string length]) //If the string is empty
        return @"";
    
    //If it start with a bracket it's a "complex operand" ex (+ 2 3)
    // we search for an operand
    unichar firstChar = [string characterAtIndex:0];
    if (firstChar == '(') {
        
        int openingBracketsCount = 0, closingBracketsCount = 0; // We now we have an operand just by checking the brackets
        
        for (int position=0; position < string.length; position++) {
            unichar ch = [string characterAtIndex:position];
            if (ch == '('){
                openingBracketsCount++;
            } else if (ch == ')'){
                closingBracketsCount++;
            }
            if (openingBracketsCount == closingBracketsCount) {
                return [string substringWithRange:(NSRange){0,position+1}];
            }
        }
        return @"";
        
    }else if ((firstChar >= '0' && firstChar<='9') || firstChar == '-'){ //If we search for a int
        
        NSString* result = [NSString new];
        
        for (int position=0; position < string.length; position++) { //we parse each character until we escape
            unichar ch = [string characterAtIndex:position];
            if ((ch >= '0' && ch<='9') || ch == '-'){
                result = [result stringByAppendingString:[NSString stringWithFormat:@"%C",ch]];
            } else if (result.length>0) {
                return result;
                
            }
        }
        return @"";
        
    }
    
    //nil if unexecpted first character
    *error = [PolishOperation cannotParse:string];
    return nil;
    
}





@end
