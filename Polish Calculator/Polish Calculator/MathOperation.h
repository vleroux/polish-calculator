//
//  MathOperation.h
//  Polish Calculator
//
//  Created by v.leroux on 12/01/15.
//  Copyright (c) 2015 vleroux. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum : NSUInteger {
    DividedByZero = -666,
    ParseError,
    UnknownOperation,
    NotANumber,
    MissingCharacter,
    NoNumber
} MathOperationError;

@interface MathOperation : NSObject

-(NSNumber*)calculateIntOperation:(unichar)operation leftOperand:(NSNumber*)left andRightOperand:(NSNumber*)right  handleError:(NSError**)error;
-(NSNumber*)calculateIntOperation:(unichar)operation arrayOfNumericalOperand:(NSArray*)operands handleError:(NSError**)error;

+(NSError*)dividedByZeroError;
+(NSError*)unknownOperation:(unichar)operation;
+(NSError*)cannotParse:(NSString*)stringToParse;
+(NSError*)notANumber:(NSString*)stringToParse;
+ (BOOL) isStringValid:(NSString*)string handleError:(NSError**)error;
@end
