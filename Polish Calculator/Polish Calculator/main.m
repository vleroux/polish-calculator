//
//  main.m
//  Polish Calculator
//
//  Created by v.leroux on 12/01/15.
//  Copyright (c) 2015 vleroux. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
