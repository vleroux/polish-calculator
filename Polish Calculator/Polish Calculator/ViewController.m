//
//  ViewController.m
//  Polish Calculator
//
//  Created by v.leroux on 12/01/15.
//  Copyright (c) 2015 vleroux. All rights reserved.
//

#import "ViewController.h"
#import "PolishOperation.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *queryTextInput;
@property (weak, nonatomic) IBOutlet UILabel *yourResultLabel;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.queryTextInput.delegate = self;
    
    [self.queryTextInput setReturnKeyType:UIReturnKeyDone];
    [self.queryTextInput setAutocorrectionType:UITextAutocorrectionTypeNo];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    //Do nothing if there is nothing to process
    if (!textField.text || !textField.text.length) {
        [self showResult:[NSNumber numberWithInt:0]];
        return;
    }
    
    NSError * error = nil;
    PolishOperation* polOp = nil;
    NSNumber *result = nil;
    @try {
        polOp = [[PolishOperation alloc]initWithQuery:textField.text];
        result = [polOp getResult:&error];
    }
    @catch (NSException *exception) {
        
        [[[UIAlertView alloc] initWithTitle:@"OUPS" message:@"Something unexpected as occured" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        return;
    }
    @finally{
        
    }
    
    if (result == nil) {
        [self showError:error];
    } else {
        [self showResult:result];
    }
    
}

-(void)showError:(NSError*)error{
    self.yourResultLabel.text = @"0";
    self.errorLabel.text = error.localizedDescription;
    self.errorLabel.alpha = 1;
    CABasicAnimation *animation =
    [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:0.05f];
    [animation setRepeatCount:5];
    [animation setAutoreverses:YES];
    [animation setFromValue:[NSValue valueWithCGPoint:
                             CGPointMake([self.errorLabel center].x - 10.0f,
                                         [self.errorLabel center].y)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                           CGPointMake([self.errorLabel center].x + 10.0f,
                                       [self.errorLabel center].y)]];
    [[self.errorLabel layer] addAnimation:animation forKey:@"position"];
}

-(void)showResult:(NSNumber*)result{
    self.errorLabel.alpha = 0;
    self.yourResultLabel.text = [NSString stringWithFormat:@"%d",result.intValue];

}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return NO;
}

@end
