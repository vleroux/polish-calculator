//
//  AppDelegate.h
//  Polish Calculator
//
//  Created by v.leroux on 12/01/15.
//  Copyright (c) 2015 vleroux. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

