//
//  MathOperation.m
//  Polish Calculator
//
//  Created by v.leroux on 12/01/15.
//  Copyright (c) 2015 vleroux. All rights reserved.
//

#import "MathOperation.h"

@implementation MathOperation

#pragma mark - Calculate
/**
 * @param operation unichar : the operation (+,-,*,/)
 * @param left NSNumber : the left operand
 * @param right NSNumber : the right operand
 * @return NSNumber : returns the calculation of the opperations
 */
-(NSNumber*)calculateIntOperation:(unichar)operation leftOperand:(NSNumber*)left andRightOperand:(NSNumber*)right handleError:(NSError**)error;{
    
    if (operation == '+') {
        return [NSNumber numberWithInt:(left.intValue + right.intValue)];
    } else if (operation == '-') {
        return [NSNumber numberWithInt:(left.intValue - right.intValue)];
    }else if (operation == '*') {
        return [NSNumber numberWithInt:(left.intValue * right.intValue)];
    }else if (operation == '/') {
        
        if (right.intValue == 0) {
            *error = [MathOperation dividedByZeroError];
            return nil;
        }
        
        return [NSNumber numberWithInt:(floor(left.intValue / right.intValue))];
    }
    
    *error = [MathOperation unknownOperation:operation];
    return nil;
}

/**
 * @param operation unichar : the operation (+,-,*,/)
 * @param operands NSArray of NSnumber : all the operands
 * @return NSNumber : returns the calculation of the opperations
 */

-(NSNumber*)calculateIntOperation:(unichar)operation arrayOfNumericalOperand:(NSArray*)operands handleError:(NSError**)error{
    
    NSNumber* result = nil;
    
    for (NSNumber* nb in operands) {
        if (!result) {
            result = nb;
        }else {
            result = [self calculateIntOperation:operation leftOperand:result andRightOperand:nb handleError:error];
        }
    }
    
    
    return result;
}

#pragma mark - Utils

+ (BOOL) isStringValid:(NSString*)string handleError:(NSError**)error{
   
    int numbersCount = 0;
    int openingBracketsCount = 0;
    int closingBracketsCount = 0;
    
    for (int position=0; position < string.length; position++) {
        unichar ch = [string characterAtIndex:position];
        if (ch == '('){
            openingBracketsCount++;
        } else if (ch == ')'){
            closingBracketsCount++;
        }
        if (ch >= '0' && ch <='9') {
            numbersCount++;
        }
    }
    
    if (numbersCount ==0) {
        *error = [MathOperation noNumberFound];
        return false;
    }else if (openingBracketsCount>closingBracketsCount) {
        *error = [MathOperation characterMustBeMissing:')'];
        return false;
    }else if (openingBracketsCount<closingBracketsCount) {
        *error = [MathOperation characterMustBeMissing:'('];
        return false;
    }
    return true;
    
    
}

#pragma mark - Errors


+(NSError*)noNumberFound{
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    [errorDetail setValue:@"I don't see any number here" forKey:NSLocalizedDescriptionKey];
    
    
    return [NSError errorWithDomain:@"vleroux" code:NoNumber userInfo:errorDetail];
}

+(NSError*)characterMustBeMissing:(unichar)c{
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    [errorDetail setValue:[NSString stringWithFormat:@"I think there is a '%C' missing somewhere",c] forKey:NSLocalizedDescriptionKey];
    
    
    return [NSError errorWithDomain:@"vleroux" code:MissingCharacter userInfo:errorDetail];
}

+(NSError*)dividedByZeroError{
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    [errorDetail setValue:@"Hopefully, I prevented you from destroying the universe" forKey:NSLocalizedDescriptionKey];
    
    
    return [NSError errorWithDomain:@"vleroux" code:DividedByZero userInfo:errorDetail];
}

+(NSError*)unknownOperation:(unichar)operation{
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    [errorDetail setValue:[NSString stringWithFormat:@"I don't know this operation : %C",operation] forKey:NSLocalizedDescriptionKey];
    
    
    return [NSError errorWithDomain:@"vleroux" code:UnknownOperation userInfo:errorDetail];
}

+(NSError*)cannotParse:(NSString*)stringToParse{
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    [errorDetail setValue:[NSString stringWithFormat:@"I don't know how to read this : %@",stringToParse] forKey:NSLocalizedDescriptionKey];
    
    
    return [NSError errorWithDomain:@"vleroux" code:ParseError userInfo:errorDetail];
}

+(NSError*)notANumber:(NSString*)stringToParse{
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    [errorDetail setValue:[NSString stringWithFormat:@"I don't think \"%@\" is a number",stringToParse] forKey:NSLocalizedDescriptionKey];
    
    
    return [NSError errorWithDomain:@"vleroux" code:NotANumber userInfo:errorDetail];
}

@end
