//
//  Polish_CalculatorTests.m
//  Polish CalculatorTests
//
//  Created by v.leroux on 12/01/15.
//  Copyright (c) 2015 vleroux. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "PolishOperation.h"
@interface Polish_CalculatorTests : XCTestCase

@end

@implementation Polish_CalculatorTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    
    //(+ 2 3) corresponds to 2 + 3 and displays 5.
    PolishOperation *test = [[PolishOperation alloc] initWithQuery:@"(+ 2 3)"];
    XCTAssert([[test getResult:nil] isEqualToNumber:[NSNumber numberWithInt:5]], @"Pass (+ 2 3)");
    
    //(* 2 (+ 3 4)) corresponds to 2 * (3 + 4) (not to 2 * 3 + 4) and displays 14.
    test = [[PolishOperation alloc] initWithQuery:@"(* 2 (+ 3 4))"];
    XCTAssert([[test getResult:nil] isEqualToNumber:[NSNumber numberWithInt:14]], @"Pass (* 2 (+ 3 4))");
    
    //(* 2 (+ 3 4) 5) corresponds to 2 * (3 + 4) * 5 and displays 70.
    test = [[PolishOperation alloc] initWithQuery:@"(* 2 (+ 3 4) 5)"];
    XCTAssert([[test getResult:nil] isEqualToNumber:[NSNumber numberWithInt:70]], @"Pass (* 2 (+ 3 4) 5)");
    
    //(- (* 6 7) (/ 8 (- 10 1))) corresponds to 6 * 7 - 8 / (10 - 1) and displays 42.
    test = [[PolishOperation alloc] initWithQuery:@"(- (* 6 7) (/ 8 (- 10 1)))"];
    XCTAssert([[test getResult:nil] isEqualToNumber:[NSNumber numberWithInt:42]], @"Pass (- (* 6 7) (/ 8 (- 10 1)))");
    
    //13 displays 13.
    test = [[PolishOperation alloc] initWithQuery:@"13"];
    XCTAssert([[test getResult:nil] isEqualToNumber:[NSNumber numberWithInt:13]], @"Pass 13 ");
    
    //(- 2 5) displays -3.
    test = [[PolishOperation alloc] initWithQuery:@"(- 2 5)"];
    XCTAssert([[test getResult:nil] isEqualToNumber:[NSNumber numberWithInt:-3]], @"Pass (- 2 5)");
    
    
    XCTAssert(YES, @"Pass all the basics");
    
    NSError *error = nil;
    
    test = [[PolishOperation alloc] initWithQuery:@"(§ 2 3)"];
    XCTAssert([test getResult:& error]== nil,@"Pass fail case ");
    NSLog(@"%@",error.localizedDescription);
    
    test = [[PolishOperation alloc] initWithQuery:@"(1 -)"];
    XCTAssert([test getResult:& error]== nil,@"Pass fail case ");
    NSLog(@"%@",error.localizedDescription);
    
    test = [[PolishOperation alloc] initWithQuery:@"(* A2 3)"];
    XCTAssert([test getResult:& error]== nil,@"Pass fail case ");
    NSLog(@"%@",error.localizedDescription);
    
    test = [[PolishOperation alloc] initWithQuery:@"(/ 4 3 0)"];
    XCTAssert([test getResult:& error]== nil,@"Pass fail case ");
    NSLog(@"%@",error.localizedDescription);

    
    test = [[PolishOperation alloc] initWithQuery:@"/ 4 3 0)"];
    XCTAssert([test getResult:& error]== nil,@"Pass fail case ");
    NSLog(@"%@",error.localizedDescription);
    
    test = [[PolishOperation alloc] initWithQuery:@"azer azer"];
    XCTAssert([test getResult:& error]== nil,@"Pass fail case ");
    NSLog(@"%@",error.localizedDescription);
    
    test = [[PolishOperation alloc] initWithQuery:@"(*- aze azr))))"];
    XCTAssert([test getResult:& error]== nil,@"Pass fail case ");
    NSLog(@"%@",error.localizedDescription);
    
    test = [[PolishOperation alloc] initWithQuery:@"Hello"];
    XCTAssert([test getResult:& error]== nil,@"We can discuss");
    NSLog(@"%@",error.localizedDescription);
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
