//
//  PolishOperation.h
//  Polish Calculator
//
//  Created by v.leroux on 12/01/15.
//  Copyright (c) 2015 vleroux. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MathOperation.h"
@interface PolishOperation : MathOperation

-(id) initWithQuery:(NSString*)query;
-(NSNumber*) getResult:(NSError**)error;

@end
